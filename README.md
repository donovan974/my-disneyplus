### Context
This project is created in 5 day.
This was a challenge to see how can i learn new technologie and implémente it fast to make somthing that look realistic.

Technologies i learnt to made this possible:
 - ExpressJS
 - Knex

Technologie i used:
 - ExpressJS
 - Knex/pg (Postgresql)
 - Tailwind (css framework) /Framermotion (animation framework)
 - React & React router dom
 - TheMovieDB api
 - Docker (Generate API env)
 - Docker-compose  (Manage API & Database env)
 - Typescript only
 - Postman (developpment)
 - axios
 - dotenv
 - jsonwebtoken
 - uuid

### Result

---

If you just want to see how it feel ! You can look down here a little gif (can take few second to load):
![](https://gitlab.com/donovan974/my-disneyplus/-/raw/main/preview.gif)
