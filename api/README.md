# API

## Usage

This api is based of a custom template and it is created to work with the front in the parent folder.
Before use it you need to create a `.env` file base on the current `env` file and complete it with the right information
<br/>
To run the api you need to install docker and be able to run docker-compose commands.

```sh
$: docker-compose up --build --remove-orphans
```

That will run the database and the api.

## Routes api

### AUTH

---

#### Sign up

```
POST /api/auth/signup
```

with the body

```json
{
    "username": "MyUsername", // Required | String
    "password": "MyEmail", // Required | String
    "email": "MyPassword" // Required | String
}
```

<br/>
<br/>

#### Sign in

```
POST /api/auth/signin
```

with the body

```json
{
    "password": "MyEmail", // Required | String
    "email": "MyPassword" // Required | String
}
```

<br/>
<br/>

#### Sign out

```
POST /api/auth/signout
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

### MOVIES

---

<br/>
<br/>

#### Get popular

```
GET /api/movies/popular
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

<br/>
<br/>

#### Get similar movie

```
GET /api/movies/similar/:film_id
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

<br/>
<br/>

#### Get all genres

```
GET /api/movies/genres
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

<br/>
<br/>

#### Get all high rated movie

```
GET /api/movies/high-rated
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

<br/>
<br/>

#### Get detail of a movie

```
GET /api/movies/details/:film_id
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

### USER

---

<br/>
<br/>

#### Get Watchlist

```
GET /api/user/watchlist/me
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

<br/>
<br/>

#### Add to watchlist

```
POST /api/user/watchlist/:film_d
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

<br/>
<br/>

#### Remove from Watchlist

```
DELETE /api/user/watchlist
```

with the header

```json
{
    "token": "[here token]" // Required | string
}
```

with the body

```json
{
    "film_id": 672 // Required | number
}
```
