import { tokenMethod, tokenStatus, tokenTime } from "./config/morgan";
import express, { Response, Request, NextFunction } from "express";
import Config from "./config/APIConfig";
import AuthRoutes from "./routes/Auth/Auth";
import morgan from "morgan";
import MoviesRoutes from "./routes/Movies/Movies";
import UserRoutes from "./routes/Users/User";

const app = express();

morgan.token("time", tokenTime);
morgan.token("my-method", tokenMethod);
morgan.token("my-status", tokenStatus);

//-- CORS
app.use((req: Request, res: Response, next: NextFunction) => {
    res.header("Access-Control-Allow-Origin", Config.API.API_CORS);
    res.header(
        "Access-Control-Allow-Methods",
        "GET, PUT, POST, DELETE, OPTIONS"
    );
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
    morgan(
        `[🔥${Config.API.API_NAME}🔥] - ⏰ :time \t| :my-status | :my-method :url \t\t\t💾 :res[content-length] octets   -   ⏱  :response-time ms`
    )
);

// === Allow Option route == \\
app.options("/*", (req: Request, res: Response) => {
    res.send("");
});

// === init routes === \\
AuthRoutes(app);
MoviesRoutes(app);
UserRoutes(app);

export default app;
