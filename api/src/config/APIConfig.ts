require("dotenv").config();

type apiProps = {
    API_NAME: string;
    API_PORT: string;
    API_CORS: string;
    API_SECRET: string;
    API_TOKEN_DURATION: number;
};

type dbProps = {
    DB_HOST: string;
    DB_PORT: number;
    DB_USER: string;
    DB_PASSWORD: string;
    DB_DATABASE: string;
};

type theMovieDb = {
    SECRET: string;
};

type configProps = {
    API: apiProps;
    DATABASE: dbProps;
    THE_MOVIE_DB: theMovieDb;
};

const Config: configProps = {
    API: {
        API_NAME: process.env.API_NAME as string,
        API_PORT: process.env.API_PORT as string,
        API_CORS: "*",
        API_SECRET: process.env.API_SECRET as string,
        API_TOKEN_DURATION: Number(process.env.API_TOKEN_DURATION),
    },
    DATABASE: {
        DB_HOST: process.env.POSTGRES_HOST as string,
        DB_PORT: Number(process.env.POSTGRES_PORT),
        DB_USER: process.env.POSTGRES_USER as string,
        DB_PASSWORD: process.env.POSTGRES_PASSWORD as string,
        DB_DATABASE: process.env.POSTGRES_DATABASE as string,
    },
    THE_MOVIE_DB: {
        SECRET: process.env.TMDB_SECRET as string,
    },
};

export default Config;
