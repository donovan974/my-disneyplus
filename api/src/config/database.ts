import Config from "./APIConfig";
import knex from "knex";

console.log(Config.DATABASE);

const initKnex = knex({
    client: "pg",
    version: "13",
    connection: {
        host: Config.DATABASE.DB_HOST,
        port: Config.DATABASE.DB_PORT,
        user: Config.DATABASE.DB_USER,
        password: Config.DATABASE.DB_PASSWORD,
        database: Config.DATABASE.DB_DATABASE,
    },
});

export default initKnex;
