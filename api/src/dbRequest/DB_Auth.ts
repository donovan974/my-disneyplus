import jwt from "jsonwebtoken";
import { DatabaseError } from "pg";
import Config from "../config/APIConfig";
import knex from "../config/database";
import { DumpError } from "../utils/Error";
import {
    returnDatabaseFormatter,
    DatabaseFormatterType,
} from "../utils/returnFormatter";

export const create_new_user = async (
    username: string,
    password: string,
    email: string
): Promise<DatabaseFormatterType> => {
    const data: DatabaseFormatterType = await knex("users")
        .insert({
            username,
            password: knex.raw(`crypt(?, gen_salt('bf'))`, [password]),
            email,
        })
        .returning("id")
        .then((user) => returnDatabaseFormatter(user, "ok"))
        .catch((error: DatabaseError) => {
            const message =
                error.code == "23505"
                    ? "user already exist"
                    : "Cannot create new_user";
            const dumpError = DumpError(message, 25, "DB_Auth.ts");
            console.log(
                "Error code = " + error.code + " and message " + error.message
            );
            return returnDatabaseFormatter(message, "ko");
        });
    return data;
};

export type TokenBody = {
    id: string;
    expire_in: number;
} | null;

export const getTokenBody = (token: string): TokenBody => {
    let body: TokenBody = null;
    let jwtDecode: jwt.JwtPayload = {};
    let returned = false;
    jwt.verify(token, Config.API.API_SECRET, (err, decoded) => {
        if (err) returned = true;
        jwtDecode = decoded as jwt.JwtPayload;
    });

    if (returned || jwtDecode.id == null) return null;
    body = {
        id: jwtDecode.id,
        expire_in: jwtDecode.expire_in,
    };

    return body;
};

export const insertNewToken = async (
    token: string,
    userId: string
): Promise<boolean> => {
    return await knex("user_tokens")
        .insert({
            token,
            user_id: userId,
            expire_in: Config.API.API_TOKEN_DURATION,
        })
        .then(() => true)
        .catch(() => {
            const dumpError = DumpError("Fail insert token", 66, "DB_Auth.ts");
            console.log(dumpError);
            return false;
        });
};

export const checkTokenValidity = async (token: string): Promise<boolean> => {
    const tokenBody = getTokenBody(token);
    if (tokenBody == null) return false;
    const data = (await knex("user_tokens")
        .select("*")
        .where("token", "=", token)
        .whereNull("expired_at")) as any;
    if (data == null || data == [] || data[0] == null) return false;
    const dateNow = Date.now();
    const tokenDate = Date.parse(data.create_at);
    const duration = data.expire_in;
    if (tokenDate + duration < dateNow) return false;
    const tokenUserId = await knex("users")
        .select("id")
        .where("id", "=", tokenBody.id);

    if (tokenUserId[0] == null || tokenUserId[0].id != data[0].user_id)
        return false;
    return true;
};

const checkUserSQL = `
SELECT * FROM users WHERE email = ? AND password = crypt(?, password)
`;

export const checkUserExistence = async (email: string, password: string) => {
    return await knex("users")
        .select("*")
        .where("email", "=", email)
        .where("password", "=", knex.raw("crypt(?, password)", [password]))
        .then((data) => {
            console.log("Data in your house: ", data);
            return returnDatabaseFormatter(data, "ok");
        })
        .catch((err: DatabaseError) =>
            returnDatabaseFormatter(
                DumpError(
                    "Error while check user existence. " + err.message,
                    101,
                    "DB_Auth.ts"
                ),
                "ko"
            )
        );
};

export const forceExpireThisToken = async (token: string) => {
    return await knex("user_tokens")
        .update({ expired_at: "NOW()" })
        .where("token", "=", token)
        .then(() => true)
        .catch(() => false);
};
