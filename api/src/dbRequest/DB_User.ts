import { DatabaseError } from "pg";
import knex from "../config/database";
import { DumpError } from "../utils/Error";
import { returnDatabaseFormatter } from "../utils/returnFormatter";

export const getUserWatchList = async (userId: string) => {
    return await knex("user_list")
        .select("film_id")
        .select("title")
        .select("image")
        .where("user_id", "=", userId)
        .then((data) => returnDatabaseFormatter(data, "ok"))
        .catch((err: DatabaseError) => {
            const dumpError = DumpError(
                "Fail to get list of user: " + userId + ". " + err,
                19,
                "DB_User.ts"
            );
            return returnDatabaseFormatter(dumpError, "ko");
        });
};

export const addFilmToWatchList = async (
    userId: string,
    film_id: number,
    image: string,
    title: string
) => {
    const list = await getUserWatchList(userId);

    if (list.status == "ko")
        return returnDatabaseFormatter("Fail to fetch user_list", "ko");

    console.log(list.data);
    let currentIdFilm: number[] = list.data.map(
        (item: { film_id: number }) => item.film_id
    );

    if (currentIdFilm.find((item: number) => item == film_id))
        return returnDatabaseFormatter("Already in list", "ok");

    return await knex("user_list")
        .insert({
            user_id: userId,
            film_id,
            image,
            title,
        })
        .then((data) => returnDatabaseFormatter(data, "ok"))
        .catch((err) => {
            const dumpError = DumpError(
                "Fail to add film to the user watchList. " + err,
                37,
                "DB_User.ts"
            );
            return returnDatabaseFormatter(dumpError, "ko");
        });
};

export const removeFilmFromWatchList = async (
    userId: string,
    film_id: number
) => {
    const list = await getUserWatchList(userId);

    if (list.status == "ko")
        return returnDatabaseFormatter("Fail to fetch user_list", "ko");

    let currentIdFilm: number[] = list.data.map(
        (item: { film_id: number }) => item.film_id
    );

    if (!currentIdFilm.find((item: number) => item == film_id))
        return returnDatabaseFormatter("not in list", "ok");
    return await knex("user_list")
        .delete("*")
        .where("user_id", "=", userId)
        .where("film_id", "=", film_id)
        .then((data) => returnDatabaseFormatter(data, "ok"))
        .catch((err) => {
            const dumpError = DumpError(
                "Fail to add film to the user watchList. " + err,
                37,
                "DB_User.ts"
            );
            return returnDatabaseFormatter(dumpError, "ko");
        });
};
