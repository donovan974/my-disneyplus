import { Request, Response } from "express";
import app from "./app";
import Config from "./config/APIConfig";

app.get("/", (req: Request, res: Response) => res.send("Root access works !"));

app.listen(Config.API.API_PORT, () => {
    console.log(
        `⚡️[server]: Server is running at https://localhost:${Config.API.API_PORT}`
    );
});
