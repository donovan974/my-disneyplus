import { Request, Response, Application as ExpressApp } from "express";
import {
    invalidTokenAnswer,
    returnApiFormatter,
    userNotExist,
} from "../../utils/returnFormatter";
import {
    checkTokenValidity,
    checkUserExistence,
    create_new_user,
    forceExpireThisToken,
    getTokenBody,
    insertNewToken,
} from "../../dbRequest/DB_Auth";
import {
    createSignIn_Body,
    createSignOut_Body,
    createSignUp_Body,
} from "./AuthValidator";
import jwt from "jsonwebtoken";
import Config from "../../config/APIConfig";
import {
    getTokenFromHeaders,
    tokenExistence,
} from "../../utils/RequestValidator";

const Auth = (app: ExpressApp) => {
    app.post("/api/auth/signup", async (req: Request, res: Response) => {
        /* Validate body */
        let data_user: any;
        let can_continue = "";
        const body = await createSignUp_Body(req, res);
        if (!body) return;

        await create_new_user(body.username, body.password, body.email)
            .then((databaseValue) => {
                if (databaseValue.status == "ko") {
                    can_continue = databaseValue.data;
                    return;
                }
                data_user = databaseValue.data;
                console.log(data_user);
            })
            .catch(
                () =>
                    (can_continue = "error appear while creating the account.")
            );

        if (can_continue !== "")
            return res
                .status(500)
                .send(returnApiFormatter({}, 500, can_continue));

        console.log("Continued");

        const userId: string = data_user[0].id;

        const access_token = jwt.sign(
            { id: userId, expiresIn: Config.API.API_TOKEN_DURATION },
            Config.API.API_SECRET,
            {
                expiresIn: Config.API.API_TOKEN_DURATION,
            }
        );
        const isTokenInsert = await insertNewToken(access_token, userId);

        if (!isTokenInsert)
            return res.send(
                returnApiFormatter(
                    {},
                    500,
                    "Account created. But an error appear while generating access token"
                )
            );
        else {
            res.send(
                returnApiFormatter(
                    {
                        access_token: access_token,
                        user_id: userId,
                        username: body.username,
                    },
                    200,
                    "ok"
                )
            );
        }
    });

    app.post("/api/auth/signin", async (req: Request, res: Response) => {
        const body = await createSignIn_Body(req, res);
        if (!body) return;

        const userData = await checkUserExistence(body.email, body.password);

        if (
            userData.status == "ko" ||
            userData.data == [] ||
            userData.data[0] == undefined
        ) {
            console.log("BONSOIR" + userData.data);
            return userNotExist(res);
        }

        const userId = userData.data[0].id;
        const access_token = jwt.sign(
            {
                id: userId,
                expiresIn: Config.API.API_TOKEN_DURATION,
            },
            Config.API.API_SECRET,
            {
                expiresIn: Config.API.API_TOKEN_DURATION,
            }
        );

        const isTokenInsert = await insertNewToken(access_token, userId);

        if (!isTokenInsert)
            return res
                .status(500)
                .send(
                    returnApiFormatter(
                        {},
                        500,
                        "Account created. But an error appear while generating access token"
                    )
                );
        res.status(200).send(
            returnApiFormatter(
                {
                    access_token,
                    user_id: userId,
                    username: userData.data[0].username,
                },
                200,
                "ok"
            )
        );
    });

    app.post("/api/auth/signout", async (req: Request, res: Response) => {
        const isTokenValid = await tokenExistence(req, res);
        if (!isTokenValid) return;
        const body = await createSignOut_Body(req, res);
        if (!body) return;
        const token = getTokenFromHeaders(req);

        const isValid = await checkTokenValidity(token);
        if (!isValid) return invalidTokenAnswer(res);

        const isSuccess = await forceExpireThisToken(token);
        if (isSuccess)
            res.status(200).send(
                returnApiFormatter("Logged out successfully", 200, "ok")
            );
        else
            res.status(500).send(
                returnApiFormatter(
                    "Fail to log out. Try to contact an administrator.",
                    500,
                    "ko"
                )
            );
    });
};

export default Auth;
