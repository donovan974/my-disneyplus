import { requestValidator } from "../../utils/RequestValidator";
import { Response, Request } from "express";

/////////////////////////////////////////////SIGN_UP/////////////////////////////////////////////////////////////

type SignUpBody = {
    username: string;
    password: string;
    email: string;
};

export const createSignUp_Body = async (req: Request, res: Response) => {
    const fieldsRequirement = {
        username: "required|string",
        password: "required|string",
        email: "required|string",
    };
    const isValidate = await requestValidator(fieldsRequirement, req, res);
    if (isValidate) return req.body as SignUpBody;
    return false;
};

/////////////////////////////////////////////SIGN_IN/////////////////////////////////////////////////////////////

type SignInBody = {
    password: string;
    email: string;
};

export const createSignIn_Body = async (req: Request, res: Response) => {
    const fieldsRequirement = {
        email: "required|string",
        password: "required|string",
    };
    const isValidate = await requestValidator(fieldsRequirement, req, res);
    if (isValidate) return req.body as SignInBody;
    return false;
};

/////////////////////////////////////////////SIGN_OUT/////////////////////////////////////////////////////////////

type SignOutBody = {
    token: string;
};

export const createSignOut_Body = async (req: Request, res: Response) => {
    const fieldsRequirement = {
        token: "required|string",
    };
    const isValidate = await requestValidator(fieldsRequirement, req, res);
    if (isValidate) return req.body as SignOutBody;
    return false;
};
