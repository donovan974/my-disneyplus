import { Application as ExpressApp, Response, Request } from "express";
import {
    invalidTokenAnswer,
    returnApiFormatter,
} from "../../utils/returnFormatter";
import { checkTokenValidity } from "../../dbRequest/DB_Auth";
import { createMovieSearch_Body } from "./MoviesValidator";
import axios from "axios";
import Config from "../../config/APIConfig";
import {
    getTokenFromHeaders,
    tokenExistence,
} from "../../utils/RequestValidator";

const Movies = (app: ExpressApp) => {
    ///////////// SEARCH A MOVIES ////////////
    app.get(
        "/api/movies/search/:query",
        async (req: Request, res: Response) => {
            const isTokenValid = await tokenExistence(req, res);
            if (!isTokenValid) return;
            const token = getTokenFromHeaders(req);

            const isValid = await checkTokenValidity(token);
            if (!isValid) return invalidTokenAnswer(res);

            const data: any = await axios({
                method: "GET",
                url: `https://api.themoviedb.org/3/search/movie?api_key=${Config.THE_MOVIE_DB.SECRET}&query=${req.params.query}&language=en-US&page=1&include_adult=false`,
            })
                .then((res) => res)
                .catch((_) => console.log("Error:" + _));

            if (data == {} || data == undefined)
                return res.send(
                    returnApiFormatter(
                        "Fail to fetch movies. Contact an administrator",
                        500,
                        "ko"
                    )
                );
            res.send(returnApiFormatter(data.data, 200, "ok"));
        }
    );

    ///////////// GET ALL GENRES OF MOVIES ////////////
    app.get("/api/movies/genre", async (req: Request, res: Response) => {
        const isTokenValid = await tokenExistence(req, res);
        if (!isTokenValid) return;
        const token = getTokenFromHeaders(req);

        const isValid = await checkTokenValidity(token);
        if (!isValid) return invalidTokenAnswer(res);

        const data: any = await axios({
            method: "GET",
            url: `https://api.themoviedb.org/3/genre/movie/list?api_key=${Config.THE_MOVIE_DB.SECRET}&language=en-US`,
        })
            .then((res) => res)
            .catch((err) => console.log("Error:" + err));

        if (data == {} || data == undefined)
            return res.send(
                returnApiFormatter(
                    "Fail to fetch genres. Contact an administrator",
                    500,
                    "ko"
                )
            );
        res.send(returnApiFormatter(data.data, 200, "ok"));
    });

    ///////////// GET ALL POPULAR MOVIES ////////////
    app.get("/api/movies/popular", async (req: Request, res: Response) => {
        const isTokenValid = await tokenExistence(req, res);
        if (!isTokenValid) return;
        const token = getTokenFromHeaders(req);

        const isValid = await checkTokenValidity(token);
        if (!isValid) return invalidTokenAnswer(res);

        const data: any = await axios({
            method: "GET",
            url: `https://api.themoviedb.org/3/movie/popular?api_key=${Config.THE_MOVIE_DB.SECRET}&language=en-US&page=1`,
        })
            .then((res) => res)
            .catch((_) => console.log("Error:" + _));

        if (data == {} || data == undefined)
            return res.send(
                returnApiFormatter(
                    "Fail to fetch popular movies. Contact an administrator",
                    500,
                    "ko"
                )
            );
        res.send(returnApiFormatter(data.data, 200, "ok"));
    });

    ///////////// GET ALL HIGH RATED MOVIES //////////// --
    app.get(
        "/api/movies/high-rated:page?",
        async (req: Request, res: Response) => {
            const isTokenValid = await tokenExistence(req, res);
            if (!isTokenValid) return;
            const token = getTokenFromHeaders(req);

            const isValid = await checkTokenValidity(token);
            if (!isValid) return invalidTokenAnswer(res);

            const page = req.params.page == undefined ? 1 : req.params.page;

            const data: any = await axios({
                method: "GET",
                url: `https://api.themoviedb.org/3/movie/top_rated?api_key=${Config.THE_MOVIE_DB.SECRET}&language=en-US&page=${page}`,
            })
                .then((res) => res)
                .catch((_) => console.log("Error:" + _));

            if (data == {} || data == undefined)
                return res
                    .status(500)
                    .send(
                        returnApiFormatter(
                            "Fail to fetch high rated movies. Contact an administrator",
                            500,
                            "ko"
                        )
                    );
            res.status(200).send(returnApiFormatter(data.data, 200, "ok"));
        }
    );

    ///////////// GET ALL SIMILAR MOVIES ////////////
    app.get(
        "/api/movies/similar/:film_id",
        async (req: Request, res: Response) => {
            const isTokenValid = await tokenExistence(req, res);
            if (!isTokenValid) return;
            const token = getTokenFromHeaders(req);

            const isValid = await checkTokenValidity(token);
            if (!isValid) return invalidTokenAnswer(res);

            const data: any = await axios({
                method: "GET",
                url: `
            https://api.themoviedb.org/3/movie/${req.params.film_id}/similar?api_key=${Config.THE_MOVIE_DB.SECRET}&language=en-US&page=1`,
            })
                .then((res) => res)
                .catch((_) => console.log("Error:" + _));

            if (data == {} || data == undefined)
                return res
                    .status(500)
                    .send(
                        returnApiFormatter(
                            "Fail to fetch similar movie. Contact an administrator",
                            500,
                            "ko"
                        )
                    );
            res.status(200).send(returnApiFormatter(data.data, 200, "ok"));
        }
    );

    ///////////// GET DETAILS OF A MOVIE ////////////
    app.get(
        "/api/movies/detail/:film_id",
        async (req: Request, res: Response) => {
            const isTokenValid = await tokenExistence(req, res);
            if (!isTokenValid) return;
            const token = getTokenFromHeaders(req);

            const isValid = await checkTokenValidity(token);
            if (!isValid) return invalidTokenAnswer(res);

            const data: any = await axios({
                method: "GET",
                url: `https://api.themoviedb.org/3/movie/${req.params.film_id}?api_key=${Config.THE_MOVIE_DB.SECRET}&language=en-US`,
            })
                .then((res) => res)
                .catch((_) => console.log("Error:" + _));

            if (data == {} || data == undefined)
                return res
                    .status(500)
                    .send(
                        returnApiFormatter(
                            "Fail to fetch details of this movie. Contact an administrator",
                            500,
                            "ko"
                        )
                    );
            res.status(200).send(returnApiFormatter(data.data, 200, "ok"));
        }
    );
};

export default Movies;
