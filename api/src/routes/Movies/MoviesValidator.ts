import {
    headerValidator,
    requestValidator,
} from "../../utils/RequestValidator";
import { Request, Response } from "express";

type MovieSearchPros = {
    query: string;
    include_adult: number;
    year: string;
    page: number;
};

export const createMovieSearch_Body = async (req: Request, res: Response) => {
    const fieldsRequirement = {
        query: "required|string",
        include_adult: "number|min:0|max:1",
        year: "string",
        page: "number",
    };
    const isValidate = await requestValidator(fieldsRequirement, req, res);
    if (isValidate) return req.body as MovieSearchPros;
    return false;
};
