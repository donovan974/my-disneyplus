import { Application as ExpressApp, Response, Request } from "express";
import {
    addFilmToWatchList,
    getUserWatchList,
    removeFilmFromWatchList,
} from "../../dbRequest/DB_User";
import { checkTokenValidity, getTokenBody } from "../../dbRequest/DB_Auth";
import {
    invalidTokenAnswer,
    returnApiFormatter,
    returnDatabaseFormatter,
} from "../../utils/returnFormatter";
import {
    createRemoveWatchListGet_Body,
    createWatchListAdd_Body,
} from "./UserValidator";
import {
    getTokenFromHeaders,
    tokenExistence,
} from "../../utils/RequestValidator";

const User = (app: ExpressApp) => {
    app.get("/api/user/watchlist/me", async (req: Request, res: Response) => {
        const isTokenExist = await tokenExistence(req, res);
        if (!isTokenExist) return;
        const token = getTokenFromHeaders(req);

        const isValid = await checkTokenValidity(token);
        if (!isValid) return invalidTokenAnswer(res);

        const tokenBody = getTokenBody(token);

        if (tokenBody == null) return invalidTokenAnswer(res);
        const db = await getUserWatchList(tokenBody.id);
        if (db.status == "ko")
            return res
                .status(500)
                .send(returnApiFormatter("Fail to fetch", 500, "ko"));
        res.status(200).send(returnApiFormatter(db.data, 200, "ok"));
    });

    app.post("/api/user/watchlist", async (req: Request, res: Response) => {
        const isTokenExist = await tokenExistence(req, res);
        if (!isTokenExist) return;
        const token = getTokenFromHeaders(req);

        const body = await createWatchListAdd_Body(req, res);
        if (!body) return;

        const isValid = await checkTokenValidity(token);
        const tokenBody = getTokenBody(token);
        if (!isValid || !tokenBody) return invalidTokenAnswer(res);

        const db = await addFilmToWatchList(
            tokenBody.id,
            body.film_id,
            body.image,
            body.title
        );
        if (db.status == "ko")
            return res
                .status(500)
                .send(
                    returnDatabaseFormatter("Fail to add to watch list.", "ko")
                );
        return res
            .status(200)
            .send(returnApiFormatter("Film added", 200, "ok"));
    });

    app.delete("/api/user/watchlist", async (req: Request, res: Response) => {
        const isTokenExist = await tokenExistence(req, res);
        if (!isTokenExist) return;
        const token = getTokenFromHeaders(req);

        const body = await createRemoveWatchListGet_Body(req, res);
        if (!body) return;

        const isValid = await checkTokenValidity(token);
        const tokenBody = getTokenBody(token);
        if (!isValid || !tokenBody) return invalidTokenAnswer(res);

        const db = await removeFilmFromWatchList(tokenBody.id, body.film_id);
        if (db.status == "ko")
            return res
                .status(500)
                .send(
                    returnDatabaseFormatter(
                        "Fail to remove from watch list.",
                        "ko"
                    )
                );
        return res
            .status(200)
            .send(returnApiFormatter("Film removed", 200, "ok"));
    });
};

export default User;
