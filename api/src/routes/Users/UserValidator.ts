import { requestValidator } from "../../utils/RequestValidator";
import { Response, Request } from "express";

/////////////////////////////////////////////WATCHLIST-ADD/////////////////////////////////////////////////////////////

type WatchListAdd = {
    film_id: number;
    image: string;
    title: string;
};

export const createWatchListAdd_Body = async (req: Request, res: Response) => {
    const fieldsRequirement = {
        film_id: "required|number",
        image: "required|string",
        title: "required|string",
    };
    const isValidate = await requestValidator(fieldsRequirement, req, res);
    if (isValidate) return req.body as WatchListAdd;
    return false;
};

/////////////////////////////////////////////WATCHLIST-REMOVE/////////////////////////////////////////////////////////////

type WatchListRemoveBody = {
    film_id: number;
};

export const createRemoveWatchListGet_Body = async (
    req: Request,
    res: Response
) => {
    const fieldsRequirement = {
        film_id: "required|number",
    };
    const isValidate = await requestValidator(fieldsRequirement, req, res);
    if (isValidate) return req.body as WatchListRemoveBody;
    return false;
};
