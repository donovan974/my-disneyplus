CREATE TABLE "users"(
    "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL UNIQUE
);

CREATE TABLE "user_list"(
    "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
    "user_id" uuid NOT NULL,
    "film_id" INTEGER NOT NULL,
    "image" TEXT NOT NULL,
    "title" TEXT NOT NULL
);

CREATE TABLE "user_tokens"(
    "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
    "token" TEXT NOT NULL,
    "user_id" uuid NOT NULL,
    "create_at" TIMESTAMPtz NOT NULL DEFAULT (NOW()),
    "expire_in" INTEGER NOT NULL,
    "expired_at" TIMESTAMPtz
);

ALTER TABLE
    "user_list" ADD CONSTRAINT "user_list_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
ALTER TABLE
    "user_tokens" ADD CONSTRAINT "user_tokens_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");