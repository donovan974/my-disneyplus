export const DumpError = (message: string, line: number, file: string) => {
    return `Error found at line ${line} in file: [${file}]. Message:${message}`;
};
