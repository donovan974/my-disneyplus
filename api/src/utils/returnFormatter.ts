import { Response } from "express";

///////////////////////////////////////////////////////DATABASE/////////////////////////////////////////////////////////////////////////
type DatabaseStatusType = "ok" | "ko";

export type DatabaseFormatterType = {
    data: any;
    status: DatabaseStatusType;
};

export const returnDatabaseFormatter = (
    data: any,
    status: DatabaseStatusType
): DatabaseFormatterType => {
    return {
        data,
        status,
    };
};

///////////////////////////////////////////////////////API/////////////////////////////////////////////////////////////////////////

type apiMessageType = "ok" | string;
export type APIFormatterType = {
    data: any;
    status: number;
    message: apiMessageType;
};

export const returnApiFormatter = (
    data: any,
    status: number,
    message: apiMessageType
): APIFormatterType => {
    return {
        data,
        status,
        message,
    };
};

export const invalidTokenAnswer = (res: Response) => {
    res.status(401).send(
        returnApiFormatter(
            {},
            401,
            "Invalid token, try to sign in. If error persist contact an administrator"
        )
    );
};

export const userNotExist = (res: Response) => {
    res.status(404).send(
        returnApiFormatter({}, 404, "User doesn't exist in the database.")
    );
};
