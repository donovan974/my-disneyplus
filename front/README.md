## Front end my-disneyplus

<br/>
<br/>

### USAGE

---

To use this front api `nodeJS` must be installed. And the API must be run. create a `.env` file based on the current `env` file and complete the field with the api adress like so:

```env
REACT_APP_BASE_API_URL=http://localhost:8082
```

after that run the following command

```shell
$: npm i
$: npm start
```

and in an other terminal (only if css has weird behaviour):

```shell
$: npm run css
```

that will build tailwind css in the `/dist`.
