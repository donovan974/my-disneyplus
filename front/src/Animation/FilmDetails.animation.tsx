import { motion, MotionProps } from "framer-motion";

type Props = React.HTMLAttributes<HTMLDivElement> &
    MotionProps & {
        hoverScale?: number;
    };

export const AnimateHoverScale = ({
    hoverScale = 1.15,
    children,
    ...other
}: Props) => {
    return (
        <motion.div {...other} whileHover={{ scale: hoverScale }}>
            {children}
        </motion.div>
    );
};

type ContentArrivalProps = React.HTMLAttributes<HTMLDivElement> & MotionProps;

export const AnimateContentFilm = ({
    children,
    ...other
}: ContentArrivalProps) => {
    return (
        <motion.div
            {...other}
            className="h-[70%] max-h-[70%] relative flex flex-col justify-center "
            initial={{ y: -700 }}
            animate={{ y: 0 }}
            transition={{
                duration: 1.5,
                type: "spring",
                bounce: 1,
                damping: 12,
            }}
        >
            {children}
        </motion.div>
    );
};

type AverageScoreProps = React.HTMLAttributes<HTMLDivElement> & MotionProps;

export const AnimateAverageScore = ({
    children,
    ...other
}: AverageScoreProps) => {
    return (
        <motion.div
            {...other}
            initial={{ y: -300 }}
            animate={{ y: 0 }}
            transition={{
                duration: 2,
                type: "spring",
                bounce: 1,
                damping: 10,
            }}
            className=" right-0 text-3xl absolute mt-28 flex justify-end pr-32"
        >
            {children}
        </motion.div>
    );
};
