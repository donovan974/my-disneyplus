import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import Home from "./Pages/Home";
import LogIn from "./Pages/LogIn";
import SignUp from "./Pages/SignUp";
import AuthContextProvider from "./Context/AuthContext";
import Movies from "./Pages/Movies";
import UserInfoContextProvider from "./Context/UserInfo";
import FilmDetail from "./Pages/FilmDetail";
import MoviesSearch from "./Pages/MoviesSearch";
import LikedMovies from "./Pages/LikedMovies";
import LogOut from "./Pages/LogOut";

console.log(process.env.REACT_APP_BASE_API_URL);

function App() {
    return (
        <div className="App bg-[#1A1D29]">
            <Router>
                <AuthContextProvider>
                    <UserInfoContextProvider>
                        <Routes>
                            <Route path="/" element={<Home />} />
                            <Route path="/home" element={<Home />} />
                            <Route path="/signin" element={<LogIn />} />
                            <Route path="/signup" element={<SignUp />} />
                            <Route path="/signout" element={<LogOut />} />
                            <Route path="/movies" element={<Movies />} />
                            <Route
                                path="/movies/liked"
                                element={<LikedMovies />}
                            />
                            <Route
                                path="/movies/search"
                                element={<MoviesSearch />}
                            />
                            <Route path="/movie/:id" element={<FilmDetail />} />
                        </Routes>
                    </UserInfoContextProvider>
                </AuthContextProvider>
            </Router>
        </div>
    );
}

export default App;
