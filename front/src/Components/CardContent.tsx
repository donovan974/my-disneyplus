import { Link, useNavigate } from "react-router-dom";
import { AiOutlineEye, AiFillHeart } from "react-icons/ai";
import { useCallback, useState } from "react";
import { motion } from "framer-motion";

export type CardContent = { src: string; id: number; title: string }[];

const Card = ({
    src,
    id,
    title,
    delayApparition,
    isLoved,
}: {
    src: string;
    id: number;
    title: string;
    delayApparition: number;
    isLoved: boolean;
}) => {
    const [isHover, setIsHover] = useState(false);

    const mouseEnter = useCallback((val: boolean) => () => setIsHover(val), []);
    const navigate = useNavigate();

    const handleNavigateClick = () => {
        navigate("/movie/" + id);
    };

    return (
        <motion.div
            initial={{ opacity: 0 }}
            animate={{
                borderColor: "#262733",
                opacity: 100,
                transition: { delay: delayApparition * 0.1, duration: 1.5 },
            }}
            whileHover={{
                scale: 1.1,
                transition: { duration: 0.3, delay: 0 },
                borderColor: "#ffffff",
            }}
            // transition={}
            className="relative bg-[#212533] w-80 h-44 cardShadow rounded-lg overflow-hidden"
            onMouseEnter={mouseEnter(true)}
            onMouseLeave={mouseEnter(false)}
        >
            <img src={src} alt={"film_tag"} className="pointer-events-none" />
            <div
                className={`absolute w-fit text-3xl  px-5 rounded-lg bottom-0 translate-x-1/2 right-1/2 mb-2 ${
                    isHover ? "block" : "hidden"
                }`}
            >
                <div
                    className="bg-white bg-opacity-50 p-1 rounded-3xl hover:bg-opacity-80 transform hover:scale-110  cursor-pointer"
                    onClick={handleNavigateClick}
                >
                    <AiOutlineEye />
                </div>
            </div>
            <div
                className={`absolute w-full px-2 h-[70%] from-[#000000] bg-gradient-to-b text-xl  top-0 translate-x-1/2 right-1/2 pt-2 font-bold text-white ${
                    isHover ? "block" : "hidden"
                }`}
            >
                {title}
            </div>
            {isLoved && (
                <div
                    className="absolute right-0 w-7 h-7 rounded-[50%] flexRowCenter text-xl bg-pink-500 top-0 m-1"
                    title="This film is in your watch list."
                >
                    <AiFillHeart className="self-center text-white" />
                </div>
            )}
        </motion.div>
    );
};

export default Card;
