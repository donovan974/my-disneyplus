import { useEffect, useState } from "react";

type Props = {
    rawContent: {
        id: number;
        logo_path: string;
        name: string;
    }[];
};

const CompanyTags = ({ rawContent }: Props) => {
    const [tags, setTags] = useState<JSX.Element[]>([]);
    useEffect(() => {
        const data = rawContent.map((item, idx) => (
            <div
                className="hoverVisibility hover:underline cursor-pointer relative"
                key={idx}
            >
                <span className="truncate">{item.name}</span>
                {item.logo_path && (
                    <img
                        src={
                            "https://image.tmdb.org/t/p/w500/" + item.logo_path
                        }
                        className="absolute visibleData bottom-0 translate-y-full w-fit bg-white p-3"
                        alt=""
                    />
                )}
            </div>
        ));
        setTags(data);
    }, [rawContent]);

    return (
        <div className=" flex space-x-4 items-center mt-3 w-full">
            <span className="font-bold">Production:</span>
            {tags}
        </div>
    );
};

export default CompanyTags;
