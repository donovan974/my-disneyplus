import React, { useEffect, useState } from "react";

type Props = {
    rawContent: { id: number; name: string }[];
};

const GenreTags = ({ rawContent }: Props) => {
    const [tags, setTags] = useState<JSX.Element[]>([]);
    useEffect(() => {
        const data = rawContent.map((item, idx) => (
            <div
                key={idx}
                className="bg-white rounded-xl w-fit px-3 py-1 text-center"
            >
                {item.name}
            </div>
        ));
        setTags(data);
    }, [rawContent]);

    return <div className="text-lg text-black flex space-x-3 ">{tags}</div>;
};

export default GenreTags;
