import React from "react";
import ControllerPic from "../assets/disneyControllerLogo.png";
import LaptopPic from "../assets/disneyLaptopLogo.png";
import PhonePic from "../assets/disneyPhoneLogo.png";
import ScreenPic from "../assets/disneyScreenLogo.png";

type Props = {
    alt: string;
    src: string;
    title: string;
    list: string[];
};

const DeviceList = ({ alt, src, title, list }: Props) => {
    return (
        <div className="flex flex-col justify-center items-center text-white">
            <img alt={alt} src={src} className="h-56" />
            <span className="text-3xl">{title}</span>
            <ul className="text-xl mt-12">
                {list.map((item, idx) => (
                    <li key={idx} className="text-center">
                        {item}
                    </li>
                ))}
            </ul>
        </div>
    );
};

const HomeDeviceDetails = () => {
    return (
        <div className="text-white flex justify-center items-center h-screen w-full">
            <div>
                <div className="text-5xl text-center font-semibold">
                    Accessible sur vos appareils préférés
                </div>
                <div className="flex flex-row-reverse items-start">
                    <DeviceList
                        src={ControllerPic}
                        alt="controller"
                        title="Consoles de jeux"
                        list={[
                            "ps4",
                            "ps5",
                            "Xbox One",
                            "Xbox Series X",
                            "Xbox Series S",
                        ]}
                    />
                    <DeviceList
                        src={PhonePic}
                        alt="Phone logo"
                        title="Mobiles et tablettes"
                        list={[
                            "Tablettes Amazon Fire",
                            "Mobiles et tablettes Android",
                            "iPhone & iPad",
                        ]}
                    />
                    <DeviceList
                        src={LaptopPic}
                        alt="Laptop logo"
                        title="Ordinateurs"
                        list={["Chrome OS", "MacOS", "PC Windows"]}
                    />
                    <DeviceList
                        src={ScreenPic}
                        alt="TV logo"
                        title="Télévisions"
                        list={[
                            "Amazon Fire TV",
                            "Appareils Android TV",
                            "Apple TV",
                            "Chromecast",
                            "TV LG",
                            "Roku",
                            "Smasung",
                        ]}
                    />
                </div>
            </div>
        </div>
    );
};

export default HomeDeviceDetails;
