import DevicePics from "../assets/disneyDevice.png";

type Props = {};

const HomeDisneyDevice = (props: Props) => {
    return (
        <div
            id="devices"
            className="h-screen  px-20 flex justify-center items-center  text-white"
        >
            <div className="flex">
                <img alt="device" src={DevicePics} className="h-[40rem]" />
                <div className="pt-28 ml-10">
                    <span className="text-4xl font-bold">
                        Regardez où et quand vous le voulez
                    </span>
                    <ul className="list-disc text-2xl ml-10 space-y-6 text-gray-300">
                        <li className="mt-6">
                            Organisez des soirées virtuelles entre amis avec
                            GroupWatch. Interrompez la vidéo, revenez sur vos
                            scènes préférées et réagissez avec six amis maximum.
                            Pour envoyer et recevoir des invitations GroupWatch,
                            un abonnement est requis
                        </li>
                        <li>
                            Téléchargez des films et des séries et regardez-les
                            hors ligne
                        </li>
                        <li>
                            Sécurisez l'accès aux contenus avec le contrôle
                            parental
                        </li>
                        <li>
                            Profitez de films et séries en 4K et son Dolby Atmos
                            sur les appareils compatibles
                        </li>
                        <li>
                            Regardez en streaming sur quatre appareils en
                            simultané
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default HomeDisneyDevice;
