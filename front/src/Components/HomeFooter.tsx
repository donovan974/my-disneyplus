import React, { useState } from "react";
import DisneyLogo from "../assets/disneyplus.svg";

type Props = {};

function HomeFooter() {
    return (
        <div className="text-white flex flex-col items-center justify-around h-screen">
            <div>
                <div className="w-full text-center text-5xl font-semibold">
                    QUESTIONS FRÉQUEMMENT POSÉES
                </div>
                <div className="w-[88vw] space-y-2 mt-10 text-xl">
                    <QuestionDetails1 />
                    <QuestionDetails2 />
                    <QuestionDetails3 />
                    <QuestionDetails4 />
                    <QuestionDetails5 />
                </div>
            </div>
            <div className="text-sm text-gray-400 flex flex-col items-center">
                <img alt="logo disney" src={DisneyLogo} className="h-10 mb-5" />
                <div className="flex flex-col  space-y-5">
                    <ul className="flex space-x-8 ">
                        <li>Conditions générales d'abonnement</li>
                        <li> Règles de Respect de la Vie Privée</li>
                        <li>Droits Données dans l'UE et au R-U</li>
                        <li>Modalités relatives aux cookies</li>
                    </ul>
                    <ul className="flex space-x-8 justify-center">
                        <li>Publicités ciblées par centres d'intérêt</li>
                        <li> Appareils compatibles</li>
                        <li>Aide</li>
                        <li>Offrez Disney+</li>
                        <li>À propos de Disney+</li>
                        <li>À propos de Disney+</li>
                    </ul>
                    <p className="text-center">
                        © 2021 Disney et ses sociétés affiliées. Tous droits
                        réservés.
                    </p>
                </div>
            </div>
        </div>
    );
}

const QuestionDetails1 = () => {
    const [isOpen, setIsOpen] = useState(false);

    const setIsOpenClick = (val: boolean) => () => setIsOpen(val);

    return (
        <div className="bg-[#13151D] p-7">
            <div className=" cursor-pointer" onClick={setIsOpenClick(!isOpen)}>
                <span className="text-xl">Qu'est-ce que Disney+ ?</span>
            </div>
            <div className={`${isOpen ? "block" : "hidden"} text-gray-300`}>
                <p className="my-6">
                    Disney+ est la destination des univers Disney, Pixar,
                    Marvel, Star Wars, National Geographic, Star et encore +.
                </p>
                <p className="mb-5">
                    Un abonnement à Disney+, c’est tout ce que vous imaginez et
                    encore + :
                </p>
                <ul className="list-disc pl-7">
                    <li>
                        Retrouvez toutes les productions originales ainsi que
                        les films, séries et courts-métrages issus des mondes
                        Disney, Pixar, Marvel, Star Wars, National Geographic,
                        Star* et encore +.
                    </li>
                    <li>
                        Téléchargez autant de films et séries que vous souhaitez
                        sur jusqu'à 10 appareils.
                    </li>
                    <li>
                        Accédez à des films et séries en 4K UHD avec Dolby
                        Vision et Dolby Atmos sur les appareils compatibles sans
                        frais supplémentaires.
                    </li>
                    <li>
                        Utilisez le contrôle parental pour sécuriser l'accès aux
                        contenus Disney+ ainsi que le profil Enfants pour les
                        plus jeunes.
                    </li>
                    <li>
                        Regardez vos contenus préférés sur 4 appareils en
                        simultané.
                    </li>
                    <li>
                        Découvrez GroupWatch, qui vous permettra de regarder à
                        distance n'importe quel film ou série avec votre famille
                        et vos proches via l'application Disney+.
                    </li>
                </ul>
            </div>
        </div>
    );
};

const QuestionDetails2 = () => {
    const [isOpen, setIsOpen] = useState(false);

    const setIsOpenClick = (val: boolean) => () => setIsOpen(val);

    return (
        <div className="bg-[#13151D] p-7">
            <div className=" cursor-pointer" onClick={setIsOpenClick(!isOpen)}>
                <span className="text-xl">
                    Que puis-je regarder sur Disney+ ?
                </span>
            </div>
            <div className={`${isOpen ? "block" : "hidden"} text-gray-300`}>
                <p className="my-6">
                    Avec des milliers de films et de séries imaginés par les
                    plus grands créateurs du monde entier et de nouveaux
                    contenus ajoutés chaque mois, vous trouverez toujours
                    quelque chose à regarder sur Disney+.
                </p>
                <ul className="list-disc pl-7 space-y-2">
                    <li>
                        Accédez aux plus beaux classiques et nouveautés des
                        studios Disney, comme Raya et le dernier dragon.{" "}
                    </li>
                    <li>
                        Découvrez des histoires émouvantes pour tous les âges,
                        créées par les génies des studios Pixar, comme
                        l'incontournable aventure italienne de l'été, Luca.
                    </li>
                    <li>
                        Vivez comme jamais l'histoire en constante évolution de
                        l'Univers Cinématographique Marvel avec des séries
                        exclusives des Studios Marvel comme WandaVision, Falcon
                        et le Soldat de l'Hiver, Hawkeye et bien sûr Loki.
                    </li>
                    <li>
                        Revivez l'épopée de la saga Skywalker ou regardez en
                        streaming la toute première série live-action Star Wars,
                        The Mandalorian.
                    </li>
                    <li>
                        Ouvrez les yeux sur la richesse de notre planète grâce
                        aux intrépides explorateurs de National Geographic et à
                        leurs documentaires passionnants.
                    </li>
                    <li>
                        Avec Star, retrouvez enfin des séries dont tout le monde
                        parle, comme Desperate Housewives, Malcolm, Les Simpson
                        ou l'intégrale de la série Futurama, imaginée par les
                        mêmes créateurs.
                    </li>
                </ul>
            </div>
        </div>
    );
};

const QuestionDetails3 = () => {
    const [isOpen, setIsOpen] = useState(false);

    const setIsOpenClick = (val: boolean) => () => setIsOpen(val);

    return (
        <div className="bg-[#13151D] p-7">
            <div className=" cursor-pointer" onClick={setIsOpenClick(!isOpen)}>
                <span className="text-xl">
                    Quel est le prix de l'abonnement Disney+ ?
                </span>
            </div>
            <div className={`${isOpen ? "block" : "hidden"} text-gray-300`}>
                <p className="my-6">
                    Disney+ est disponible pour 8,99 € par mois. Vous pouvez
                    aussi économiser plus de 15 %* en vous abonnant pour 12 mois
                    au prix de 89,90 €.
                </p>
                <p className="my-6">
                    Vous pouvez également offrir un an de Disney+ à un ami ou un
                    membre de votre famille en cliquant{" "}
                    <a
                        href="https://www.disneyplus.com/fr-fr/welcome/gift-subscription"
                        className="underline text-white"
                    >
                        ici
                    </a>
                    .
                </p>
                <p className="my-6">
                    * Voir conditions d’abonnement. Comparatif entre 12 mois
                    d'abonnement mensuel et l'abonnement annuel.
                </p>
            </div>
        </div>
    );
};

const QuestionDetails4 = () => {
    const [isOpen, setIsOpen] = useState(false);

    const setIsOpenClick = (val: boolean) => () => setIsOpen(val);

    return (
        <div className="bg-[#13151D] p-7">
            <div className=" cursor-pointer" onClick={setIsOpenClick(!isOpen)}>
                <span className="text-xl">
                    Quels appareils et plateformes puis-je utiliser avec Disney+
                    ?
                </span>
            </div>
            <div className={`${isOpen ? "block" : "hidden"} text-gray-300`}>
                <p className="my-6">
                    Disney+ est accessible sur mobiles, navigateurs Web,
                    consoles de jeux, décodeurs TV et TV connectées. Cliquez ici
                    pour retrouver la liste complète des appareils compatibles.
                </p>
            </div>
        </div>
    );
};

const QuestionDetails5 = () => {
    const [isOpen, setIsOpen] = useState(false);

    const setIsOpenClick = (val: boolean) => () => setIsOpen(val);

    return (
        <div className="bg-[#13151D] p-7">
            <div className=" cursor-pointer" onClick={setIsOpenClick(!isOpen)}>
                <span className="text-xl">
                    L'abonnement Disney+ comprend-il une durée d'engagement ?
                </span>
            </div>
            <div className={`${isOpen ? "block" : "hidden"} text-gray-300`}>
                <p className="my-6">
                    Disney+ est sans engagement et vous pouvez annuler votre
                    abonnement quand vous le souhaitez. L'annulation sera
                    effective à la fin de la période pour laquelle vous avez
                    payé. Pour vous désabonner, suivez ces 5 étapes:
                </p>
                <ol className="list-decimal pl-7">
                    <li>Identifiez-vous sur www.DisneyPlus.com.</li>
                    <li>Sélectionnez votre profils.</li>
                    <li>Sélectionnez Compte</li>
                    <li>Sélectionnez votre abonnement</li>
                    <li>Sélectionnez Résilier l’abonnement</li>
                    <li>Sélectionnez Continuer pour résilier</li>
                    <li>Sélectionnez Continuer pour résilier</li>
                    <li>
                        Pour plus d'informations, cliquez{" "}
                        <a
                            href="https://help.disneyplus.com/csp?id=csp_article_content&sys_kb_id=fd73b967dbeb8c98c2deeacb1396190e"
                            className="underline text-white"
                        >
                            ici
                        </a>
                        .
                    </li>
                </ol>
            </div>
        </div>
    );
};

export default HomeFooter;
