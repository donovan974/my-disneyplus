import React from "react";

type Props = {
    price: string;
    duration: string;
    desc: string;
    buttonText: string;
};

const HomePricing = ({ price, duration, desc, buttonText }: Props) => {
    return (
        <div className="w-[331px] p-2 space-y-3">
            <p>
                <span className="text-white mr-2 text-2xl font-bold">
                    {price}
                </span>
                <span>{duration}</span>
            </p>
            <p>{desc}</p>
            <div className="py-3 cursor-pointer bg-[#0063e5] text-center rounded-[0.25rem] text-white uppercase text-xl">
                {buttonText}
            </div>
        </div>
    );
};

export default HomePricing;
