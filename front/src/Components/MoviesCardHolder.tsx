import React, { useCallback, useRef, useState } from "react";
import { motion } from "framer-motion";
import { useUserInfoContext } from "../Context/UserInfo";
import Card from "./CardContent";

type Props = {
    title: string;
    content?: { src: string; id: number; title: string }[];
};

function MoviesCardHolder({ title, content }: Props) {
    const containerRef = useRef(null);
    const { isInList } = useUserInfoContext();

    const contentList = useCallback(() => {
        if (content === null || content === undefined || content === [])
            return <></>;
        return content.map((item, idx) => {
            return (
                <Card
                    key={idx}
                    src={item.src}
                    id={item.id}
                    title={item.title}
                    delayApparition={idx}
                    isLoved={isInList(item.id)}
                />
            );
        });
    }, [content]);

    return (
        <div
            ref={containerRef}
            className="w-full pl-24 cursor-default h-[17rem] overflow-hidden mt-3 bg-red-s400 relative"
        >
            <div className="mb-5">
                <span className="text-white text-3xl">{title}</span>
            </div>
            <motion.div
                className="h-full w-fit flex space-x-7"
                drag={"x"}
                dragConstraints={containerRef}
            >
                {contentList()}
            </motion.div>
        </div>
    );
}

export default MoviesCardHolder;
