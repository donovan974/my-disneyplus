import React, { useState, useCallback, useRef } from "react";
import DisneyCard from "../assets/disneyCard.png";
import pixarCard from "../assets/pixarCard.png";
import StarCard from "../assets/starCard.png";
import NationalGeoCard from "../assets/nationalGeoCard.png";
import MarvelCard from "../assets/marvelCard.png";
import starWarsCard from "../assets/starWarsCard.png";
import { motion } from "framer-motion";

type Props = {};

type CompanyCardProps = {
    videoSrc: string;
    imgSrc: string;
    alt: string;
};

const CompanyCard = ({ videoSrc, imgSrc, alt }: CompanyCardProps) => {
    const [isHover, setIsHover] = useState(false);

    const mouseEnter = useCallback((val: boolean) => () => setIsHover(val), []);

    return (
        <motion.div
            whileHover={{
                scale: 1.1,
                transition: { duration: 0.3 },
                borderColor: "#ffffff",
            }}
            className="relative h-36 w-[16rem] border-gray-400 to-[#262733] bg-[#262733] border-[3px] rounded-xl overflow-hidden cardShadow cursor-pointer"
            onMouseEnter={mouseEnter(true)}
            onMouseLeave={mouseEnter(false)}
        >
            <div className={`${isHover ? "visible" : "invisible"}`}>
                <video width="320" height="240" loop autoPlay playsInline muted>
                    <source src={videoSrc} type="video/mp4" />
                </video>
            </div>
            <img src={imgSrc} alt={alt} className="absolute top-0 left-0" />
        </motion.div>
    );
};

const MoviesCompanyCategory = (props: Props) => {
    return (
        <div className="flex justify-center space-x-10 px-20">
            <CompanyCard
                videoSrc="https://vod-bgc-eu-west-1.media.dssott.com/bgui/ps01/disney/bgui/2019/08/01/1564674844-disney.mp4"
                alt="Disney card"
                imgSrc={DisneyCard}
            />
            <CompanyCard
                videoSrc="https://vod-bgc-eu-west-1.media.dssott.com/bgui/ps01/disney/bgui/2019/08/01/1564676714-pixar.mp4"
                alt="pixarCard"
                imgSrc={pixarCard}
            />
            <CompanyCard
                videoSrc="https://vod-bgc-eu-west-1.media.dssott.com/bgui/ps01/disney/bgui/2019/08/01/1564676115-marvel.mp4"
                alt="MarvelCard"
                imgSrc={MarvelCard}
            />
            <CompanyCard
                videoSrc="https://vod-bgc-eu-west-1.media.dssott.com/bgui/ps01/disney/bgui/2020/12/17/1608229455-star-wars.mp4"
                alt="starWarsCard"
                imgSrc={starWarsCard}
            />
            <CompanyCard
                videoSrc="https://vod-bgc-eu-west-1.media.dssott.com/bgui/ps01/disney/bgui/2019/08/01/1564676296-national-geographic.mp4"
                alt="NationalGeoCard"
                imgSrc={NationalGeoCard}
            />
            <CompanyCard
                videoSrc="https://vod-bgc-eu-west-1.media.dssott.com/bgui/ps01/disney/bgui/2020/12/17/1608169994-brand-star.mp4"
                alt="StarCard"
                imgSrc={StarCard}
            />
        </div>
    );
};

export default MoviesCompanyCategory;
