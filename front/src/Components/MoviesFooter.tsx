import React from "react";
import DisneyLogo from "../assets/disneyplus.svg";

type Props = {};

const MoviesFooter = (props: Props) => {
    return (
        <div className="text-sm text-gray-400 flex flex-col items-center">
            <img alt="logo disney" src={DisneyLogo} className="h-10 mb-5" />
            <div className="flex flex-col  space-y-5">
                <ul className="flex space-x-8 ">
                    <li>Conditions générales d'abonnement</li>
                    <li> Règles de Respect de la Vie Privée</li>
                    <li>Droits Données dans l'UE et au R-U</li>
                    <li>Modalités relatives aux cookies</li>
                </ul>
                <ul className="flex space-x-8 justify-center">
                    <li>Publicités ciblées par centres d'intérêt</li>
                    <li> Appareils compatibles</li>
                    <li>Aide</li>
                    <li>Offrez Disney+</li>
                    <li>À propos de Disney+</li>
                    <li>À propos de Disney+</li>
                </ul>
                <p className="text-center">
                    © 2021 Disney et ses sociétés affiliées. Tous droits
                    réservés.
                </p>
            </div>
        </div>
    );
};

export default MoviesFooter;
