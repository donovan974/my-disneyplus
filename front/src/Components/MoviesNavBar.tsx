import {
    AiFillHome,
    AiFillStar,
    AiOutlineHeart,
    AiOutlinePoweroff,
} from "react-icons/ai";
import { HiSearch } from "react-icons/hi";
import { GiFilmProjector } from "react-icons/gi";
import { RiTvFill } from "react-icons/ri";
import DisneyLogo from "../assets/disneyplus.svg";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AnimatePresence, motion } from "framer-motion";
import { useAuthContext } from "../Context/AuthContext";

type Props = {};

const NavListElement = ({
    icon,
    title,
    to,
}: {
    icon: JSX.Element;
    title: string;
    to?: string;
}) => {
    const [isHover, setIsHover] = useState(false);

    const hoverFunc = (val: boolean) => () => setIsHover(val);
    return (
        <motion.li
            onMouseEnter={hoverFunc(true)}
            onMouseLeave={hoverFunc(false)}
            whileHover={{ scale: 1.1 }}
        >
            <Link to={to === undefined ? "/movies" : to}>
                <div className="flex items-center text-mg font-semibold">
                    {icon} <span className="ml-3 uppercase">{title}</span>
                </div>
            </Link>
            <AnimatePresence>
                {isHover && (
                    <motion.div
                        initial={{ width: "0%" }}
                        animate={{
                            width: "100%",
                            transition: { duration: 0.2 },
                        }}
                        className="border-b border-white"
                    />
                )}
            </AnimatePresence>
        </motion.li>
    );
};

const MoviesNavBar = (props: Props) => {
    const { auth } = useAuthContext();
    const navigate = useNavigate();
    const handleLogout = () => navigate("/signout");
    return (
        <nav className="px-10 h-[7%] flex items-center justify-between fixed z-10 w-full bg-[#090B13]">
            <ul className="text-white flex space-x-10  h-full items-center">
                <Link to={"/movies"}>
                    <li className="mr-5">
                        <img
                            className="h-10"
                            src={DisneyLogo}
                            alt="disneyLogo"
                        />
                    </li>
                </Link>
                <NavListElement icon={<AiFillHome />} title="Home" />
                <NavListElement
                    icon={<HiSearch />}
                    title="Search"
                    to="/movies/search"
                />
                <NavListElement
                    icon={<AiOutlineHeart />}
                    title="Likes"
                    to="/movies/liked"
                />
                <NavListElement icon={<AiFillStar />} title="Originals" />
                <NavListElement icon={<GiFilmProjector />} title="film" />
                <NavListElement icon={<RiTvFill />} title="series" />
            </ul>
            <div className="text-lg text-white h-full flex items-center relative hoverVisibility">
                <span>{auth.username ? auth.username : "username"}</span>
                <div className="rounded-[50%] h-12 w-12 bg-red-400 ml-2"></div>
                <div
                    className="h-14 w-fit bg-[#090B13] absolute bottom-0 translate-y-full visibleData flex items-center p-4 hover:text-red-500 cursor-pointer"
                    onClick={handleLogout}
                >
                    <div>
                        <AiOutlinePoweroff className="text-5xl stroke-[3rem] self-center" />
                    </div>

                    <div>Disconnect</div>
                </div>
            </div>
        </nav>
    );
};

export default MoviesNavBar;
