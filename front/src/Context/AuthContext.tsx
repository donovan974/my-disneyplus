import React, { createContext, useContext, useState } from "react";
import { useLocalStorage } from "../Utils/LocalStorage";

type Props = { children: React.ReactChild };

export const AuthContext = createContext<{
    auth: AuthProps;
    setAuth: (token: string, user_id: string, username: string) => void;
}>({
    auth: { token: "", userId: "", username: "" },
    setAuth: (token: string, user_id: string, username: string) => {},
});

type AuthProps = {
    token: string;
    userId: string;
    username: string;
};

export const useAuthContext = () => {
    const auth = useContext(AuthContext);
    return { auth: auth.auth, setAuth: auth.setAuth };
};

const AuthContextProvider = ({ children }: Props) => {
    const [token, setToken] = useLocalStorage("access_token");
    const [userId, setUserId] = useLocalStorage("user_id");
    const [userName, setUserName] = useLocalStorage("username");
    const [auth, setAuthValue] = useState<AuthProps>({
        token: token,
        userId: userId,
        username: userName,
    });

    console.log();

    const setAuth = (_token: string, _user_id: string, _username: string) => {
        setToken(_token);
        setUserId(_user_id);
        setUserName(_username);
        setAuthValue({ token: _token, userId: _user_id, username: _username });
    };

    return (
        <AuthContext.Provider value={{ auth, setAuth }}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthContextProvider;
