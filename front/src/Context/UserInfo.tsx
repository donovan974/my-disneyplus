import React, { createContext, useContext, useEffect, useState } from "react";
import { useAuthContext } from "./AuthContext";
import { ErrorApiProps } from "../Request/MoviesRequest";
import { useExpiredSession } from "../Utils/ExpiredSession";
import { getWatchList, WatchListProps } from "../Request/UserRequest";

type Props = { children: React.ReactChild };

export const UserInfoContext = createContext<{
    userInfo: UserInfoProps;
    refreshWatchList: () => void;
}>({
    userInfo: { watchList: [] },
    refreshWatchList: () => {},
});

type UserInfoProps = {
    watchList: WatchListProps[];
};

export const useUserInfoContext = () => {
    const userinfo = useContext(UserInfoContext);
    const func = (val: number) => {
        const it = userinfo.userInfo.watchList.find(
            (item) => item.film_id === val
        );
        return it !== undefined;
    };
    return {
        userInfo: userinfo.userInfo,
        refresh: userinfo.refreshWatchList,
        isInList: func,
    };
};

const UserInfoContextProvider = ({ children }: Props) => {
    const [userInfo, setAuthValue] = useState<UserInfoProps>({
        watchList: [],
    });
    const expire_session = useExpiredSession();

    const refresh = () => {
        getWatchList().then((data) => {
            if ("error_message" in data && data.error_code === 401)
                expire_session();
            if (data instanceof Array) {
                const arr = data as WatchListProps[];
                setAuthValue({
                    watchList: arr,
                });
                console.log(data);
            }
        });
    };

    useEffect(() => refresh(), []);

    return (
        <UserInfoContext.Provider
            value={{ userInfo, refreshWatchList: refresh }}
        >
            {children}
        </UserInfoContext.Provider>
    );
};

export default UserInfoContextProvider;
