import { useUserInfoContext } from "../Context/UserInfo";
import { addWatchList, deleteWatchList } from "../Request/UserRequest";
import {
    getPosterImage,
    PosterObject,
    useMovieDetail,
    useSimilarMovie,
} from "../Utils/MoviesApiHooks";
import { useCallback, useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { ErrorApiProps, MovieDetailsProps } from "../Request/MoviesRequest";

import { CardContent } from "../Components/CardContent";
const useFilmDetails = () => {
    const { id } = useParams();
    let film_id = parseInt(id || "-1");
    const [movieDetail, detailStatus, refreshDetails] = useMovieDetail(film_id);
    const [similar, statusSimilar, refreshSimilar] = useSimilarMovie(film_id);
    const [similarFilm, setSimilarFilm] = useState<CardContent>([]);
    const { isInList, refresh } = useUserInfoContext();
    const [isLoved, setIsLoved] = useState(isInList(film_id));
    const [filmDetail, setFilmDetail] = useState<
        MovieDetailsProps | null | string
    >(null);
    const addOrRemoveInWatchList = useCallback(
        async (val: boolean) => {
            let data: boolean | ErrorApiProps;

            const film = filmDetail as MovieDetailsProps;
            const full_image = getPosterImage(film as PosterObject);
            if (val) data = await addWatchList(film_id, full_image, film.title);
            else data = await deleteWatchList(film_id);

            if (data === true) {
                refresh();
                setIsLoved(val);
            }
        },
        [film_id, filmDetail, refresh]
    );

    useEffect(() => {
        refreshDetails();
        refreshSimilar();
        setIsLoved(isInList(film_id));
    }, [id]);

    useEffect(() => {
        if (statusSimilar === "DONE") setSimilarFilm((_) => similar);
    }, [statusSimilar]);

    useEffect(() => {
        if (detailStatus === "DONE") setFilmDetail(movieDetail);
        else if (detailStatus === "LOAD") setFilmDetail(null);
        else if (detailStatus === "ERROR") setFilmDetail("ERROR");
        return () => {};
    }, [detailStatus]);

    return [filmDetail, isLoved, addOrRemoveInWatchList, similarFilm] as [
        string | MovieDetailsProps | null,
        boolean,
        (val: boolean) => Promise<void>,
        CardContent
    ];
};

export default useFilmDetails;
