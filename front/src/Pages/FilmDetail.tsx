import Loader from "../Components/Loader";
import MoviesCardHolder from "../Components/MoviesCardHolder";
import MoviesNavBar from "../Components/MoviesNavBar";
import { MovieDetailsProps } from "../Request/MoviesRequest";
import { getPosterImage, PosterObject } from "../Utils/MoviesApiHooks";
import GenreTags from "../Components/GenresTags";
import CompanyTags from "../Components/CompanyTags";
import { AiFillHeart } from "react-icons/ai";
import {
    AnimateAverageScore,
    AnimateContentFilm,
    AnimateHoverScale,
} from "../Animation/FilmDetails.animation";
import useFilmDetails from "../Hooks/useFilmDetails";

const FilmDetail = () => {
    const [filmDetail, isLoved, addOrRemoveInWatchList, similarFilm] =
        useFilmDetails();

    if (filmDetail === null || filmDetail === "ERROR")
        return (
            <div className=" h-screen w-screen ">
                <MoviesNavBar />
                <div className="w-full h-full flex justify-center items-center relative">
                    {filmDetail === "ERROR" ? (
                        <div className=" text-center font-bold text-3xl text-red-600">
                            Sorry this film is currently unaccessible. <br />
                            Try to reload the page or contact an administrator.
                        </div>
                    ) : (
                        <Loader />
                    )}
                </div>
            </div>
        );

    const film = filmDetail as MovieDetailsProps;

    return (
        <div className="w-screen h-screen overflow-hidden relative text-[#ffffff]">
            <img
                src={getPosterImage(film as PosterObject)}
                alt="Poster film"
                className="w-full absolute"
            />
            <div className="absolute top-0 w-full h-full bg-black bg-opacity-50" />
            <MoviesNavBar />
            <AnimateAverageScore>
                <div className="text-black font-bold bg-gradient-to-b from-emerald-300 to-emerald-500  w-24 flex justify-center items-center h-24 rounded-[50%]">
                    {film.vote_average * 10}%
                </div>
            </AnimateAverageScore>
            <div className="w-full h-full">
                <AnimateContentFilm>
                    <div className="w-[70%] pl-10">
                        <span className="text-5xl font-bold">{film.title}</span>
                        <div className="text-3xl font-bold flex mt-3">
                            <span>{film.release_date.substring(0, 4)}</span>
                            <div className="ml-5">
                                <GenreTags rawContent={film.genres} />
                            </div>
                        </div>
                        <p className="italic mt-10 w-[70%] hover:font-bold">
                            {film.overview}
                        </p>
                        <CompanyTags rawContent={film.production_companies} />
                        <AnimateHoverScale
                            onClick={() => addOrRemoveInWatchList(true)}
                            className="mt-10 cursor-pointer bg-white rounded-[50%] text-7xl text-black uppercase w-fit h-fit font-bold px-2 flexRowCenter"
                        >
                            {!isLoved ? (
                                <AiFillHeart className="stroke-2 self-center text-black" />
                            ) : (
                                <AiFillHeart className="stroke-2 self-center text-pink-500" />
                            )}
                        </AnimateHoverScale>
                    </div>
                </AnimateContentFilm>
                <MoviesCardHolder content={similarFilm} title="Similar" />
            </div>
        </div>
    );
};

export default FilmDetail;
