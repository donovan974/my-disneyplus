import DisneyLogo from "../assets/disneyplus.svg";
import HomeFooter from "../Components/HomeFooter";
import HomeDeviceDetails from "../Components/HomeDeviceDetails";
import HomeDisneyDevice from "../Components/HomeDisneyDevice";
import HomePricing from "../Components/HomePricing";
import { Link } from "react-router-dom";

type Props = {};

const Home = (props: Props) => {
    return (
        <div className=" overflow-hidden bg-[#040814]">
            <div className="h-screen backgroundDisney">
                <nav>
                    <ul className="flex w-full justify-end p-4 pr-10">
                        <li className="uppercase cursor-point text-white bg-black border border-white text-xl px-5 py-2 hover:shadow-lg hover:bg-white hover:text-black hover:border-black">
                            <Link to="/signin">S'identifier</Link>
                        </li>
                    </ul>
                </nav>
                <div className=" text-white w-[40%] ml-32 pt-[20vh]">
                    <img
                        src={DisneyLogo}
                        alt="disney logo"
                        className="h-[97px]"
                    />
                    <p className="text-5xl font-semibold mt-10">
                        Les histoires que vous imaginez <br />+ Beaucoup
                        d’autres
                    </p>
                    <div className="flex text-gray-400 mt-10">
                        <HomePricing
                            price="8,99€"
                            duration="par mois"
                            desc="Sans engagement*. Voir conditions d'abonnemen"
                            buttonText="S'INSCRIRE"
                        />
                        <HomePricing
                            price="89,90€"
                            duration="pour un an"
                            desc="Économisez plus de 15%**. Voir conditions
                                d’abonnement."
                            buttonText="économiser sur 12 mois"
                        />
                    </div>
                    <p className="text-gray-400 text-sm ">
                        * Résiliation effective à la fin de la période de
                        facturation en cours. <br />
                        ** Comparatif entre 12 mois d'abonnement mensuel et
                        l'abonnement annuel.
                    </p>
                </div>
            </div>
            <HomeDisneyDevice />
            <div className="w-full flex justify-center my-28">
                <div className="py-3 cursor-pointer w-fit px-32 bg-[#0063e5] text-center rounded-[0.25rem] text-white uppercase text-xl">
                    S'INSCRIRE
                </div>
            </div>
            <HomeDeviceDetails />
            <HomeFooter />
        </div>
    );
};

export default Home;
