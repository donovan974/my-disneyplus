import React from "react";
import Card from "../Components/CardContent";
import MoviesFooter from "../Components/MoviesFooter";
import MoviesNavBar from "../Components/MoviesNavBar";
import { useUserInfoContext } from "../Context/UserInfo";

type Props = {};

const LikedMovies = (props: Props) => {
    const { userInfo } = useUserInfoContext();

    return (
        <div className="w-screen h-screen overflow-auto">
            <div className="h-[7%]">
                <MoviesNavBar />
            </div>
            <div className="text-white w-full px-14 pt-5 flex flex-col justify-between h-[82%]">
                <div>
                    <span className="text-4xl font-bold">Liked movies</span>
                    <div className="grid grid-cols-5 mt-5 gap-7 mb-10">
                        {userInfo.watchList.map((item, idx) => (
                            <Card
                                title={item.title}
                                src={item.image}
                                key={idx}
                                delayApparition={idx}
                                id={item.film_id}
                                isLoved={false}
                            />
                        ))}
                    </div>
                </div>
                <MoviesFooter />
            </div>
        </div>
    );
};

export default LikedMovies;
