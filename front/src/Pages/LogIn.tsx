import { FormEvent, useCallback, useEffect, useRef, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import DisneyLogo from "../assets/disneyplus.svg";
import { SignIn } from "../Request/AuthRequest";
import { IoWarningOutline } from "react-icons/io5";
import { useAuthContext } from "../Context/AuthContext";

type Props = {};

function LogIn({}: Props) {
    const signInData = useRef<{
        email: string;
        password: string;
    }>({ email: "", password: "" });
    const { auth, setAuth } = useAuthContext();
    const location = useLocation();
    const navigate = useNavigate();

    const [message, setMessage] = useState<string>(
        (location.state as string) || ""
    );

    useEffect(() => {
        auth.token !== "" && navigate("/movies");
    }, []);

    const request = useCallback(() => {
        SignIn(signInData.current.email, signInData.current.password).then(
            (data) => {
                if (data.token === "") {
                    setMessage(
                        "Wrong e-mail or password. If problem persist contact an administration"
                    );
                    return;
                }
                setAuth(data.token, data.user_id, data.username);
                navigate("/movies");
            }
        );
    }, []);

    const inputHandler =
        (itemName: "email" | "password") =>
        (e: React.ChangeEvent<HTMLInputElement>) =>
            (signInData.current[itemName] = e.target.value);

    const submitHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    };

    return (
        <div className="w-full h-screen overflow-hidden flex justify-center items-start">
            <div className="w-[23rem] space-y-8 text-white mt-12">
                <div className="w-full flex justify-center items-center">
                    <Link to="/home">
                        <img
                            alt="Disney Logo"
                            className="h-[5.5rem]"
                            src={DisneyLogo}
                        />
                    </Link>
                </div>
                <div className="text-2xl leading-7 font-semibold">
                    Identifiez-vous avec votre adresse e-mail
                </div>
                <div
                    className={`${
                        message !== "" ? "block" : "hidden"
                    } w-full py-2 bg-yellow-200 text-red-500 font-semibold px-2 border-black border-2 flex items-center`}
                >
                    <IoWarningOutline className="mr-2 text-xl stroke-2" />
                    {message}
                </div>
                <form className="space-y-5" onSubmit={submitHandler}>
                    <input
                        type="text"
                        placeholder="Adress e-mail"
                        className="outline-none w-full bg-[#31343E] p-3"
                        onChange={inputHandler("email")}
                    />
                    <input
                        type="password"
                        placeholder="Password"
                        className="outline-none w-full bg-[#31343E] p-3"
                        onChange={inputHandler("password")}
                    />
                    <button
                        className="py-3 bg-[#068DFF] w-full cursor-pointer font-semibold text-center rounded-[0.25rem] text-white uppercase text-xl"
                        onClick={request}
                    >
                        SE CONNECTER
                    </button>
                </form>
                <p>
                    Nouveau sur Disney+ ?{" "}
                    <Link to="/signup" className="underline">
                        S'inscrire
                    </Link>
                </p>
            </div>
        </div>
    );
}

export default LogIn;
