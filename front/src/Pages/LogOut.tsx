import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../Context/AuthContext";
import { signOut } from "../Request/AuthRequest";

type Props = {};

const LogOut = (props: Props) => {
    const { setAuth } = useAuthContext();
    const navigate = useNavigate();

    useEffect(() => {
        signOut();
        setAuth("", "", "");
        const id = setTimeout(() => navigate("/home"), 1000);
        return () => clearTimeout(id);
    }, []);

    return (
        <div className="w-screen h-screen text-center">
            {" "}
            You will bee soon redirect to home page...{" "}
        </div>
    );
};

export default LogOut;
