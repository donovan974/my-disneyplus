import React, { useEffect, useState } from "react";
import MoviesCardHolder from "../Components/MoviesCardHolder";
import MoviesCompanyCategory from "../Components/MoviesCompanyCategory";
import MoviesNavBar from "../Components/MoviesNavBar";
import { useAuthContext } from "../Context/AuthContext";
import { useNavigate } from "react-router-dom";
import { usePopularMovie, useTopRatedMovie } from "../Utils/MoviesApiHooks";
import MoviesFooter from "../Components/MoviesFooter";
import { CardContent } from "../Components/CardContent";

type Props = {};

const Movies = (props: Props) => {
    const { auth } = useAuthContext();
    const navigate = useNavigate();
    const [popular, statusPopular] = usePopularMovie();
    const [top, statusTop] = useTopRatedMovie();
    const [popularFilm, setPopularFilm] = useState<CardContent>([]);
    const [topFilm, setTopFilm] = useState<CardContent>([]);

    useEffect(() => {
        if (auth.token === "") navigate("/home");
    }, []);

    useEffect(() => {
        if (statusPopular === "DONE") setPopularFilm((_) => popular);
    }, [popular, statusPopular]);

    useEffect(() => {
        if (statusTop === "DONE") setTopFilm((_) => top);
    }, [top, statusTop]);

    return (
        <div className="w-screen h-screen overflow-auto pb-10">
            <MoviesNavBar />
            <div className=" mt-[7%]" />
            <MoviesCompanyCategory />
            <MoviesCardHolder title="Popular" content={popularFilm} />
            <MoviesCardHolder title="High rated" content={topFilm} />
            <MoviesFooter />
        </div>
    );
};

export default Movies;
