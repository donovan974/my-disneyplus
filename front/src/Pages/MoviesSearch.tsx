import React, { useCallback, useEffect, useState } from "react";
import Card from "../Components/CardContent";
import Loader from "../Components/Loader";
import MoviesFooter from "../Components/MoviesFooter";
import MoviesNavBar from "../Components/MoviesNavBar";
import { useUserInfoContext } from "../Context/UserInfo";
import useDebounce from "../Request/useDebonceValue";
import { useSearchMovie } from "../Utils/MoviesApiHooks";

const MoviesSearch = () => {
    const [searchCardData, setSearchCardData] = useState<JSX.Element[]>([]);
    const [query, setQuery] = useState("_");
    const debouncedQuery = useDebounce(query, 1000);
    const [searchedMovie, status, searchFunction] = useSearchMovie();
    const { isInList } = useUserInfoContext();

    useEffect(() => {
        if (status === "DONE")
            setSearchCardData(() => {
                const data = searchedMovie.map((item, idx) => (
                    <Card
                        title={item.title}
                        src={item.src}
                        id={item.id}
                        key={idx}
                        delayApparition={idx}
                        isLoved={isInList(item.id)}
                    />
                ));
                return data;
            });
        else if (status === "LOAD" || status === "ERROR") setSearchCardData([]);
    }, [status]);

    useEffect(() => {
        searchFunction(query);
    }, [debouncedQuery]);

    const inputHandler = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => setQuery(e.target.value),
        []
    );

    return (
        <div className="relative w-screen h-screen overflow-auto text-white">
            <div className="h-[18%]">
                <MoviesNavBar />
                <input
                    type="text"
                    className="h-24 w-full bg-gradient-to-t from-[#4B4E5A] to to-[#31333B] mt-16 focus:outline-none px-20 text-4xl caret-white"
                    placeholder="  Enter movie title.."
                    autoFocus
                    onChange={inputHandler}
                />
            </div>
            <div className="overflow-auto h-[82%]">
                <div className="w-full">
                    {status === "LOAD" ? (
                        <div className="w-full flex justify-center items-center">
                            <Loader />
                        </div>
                    ) : status === "ERROR" ? (
                        <div className=" mt-20 mb-32 text-center font-bold text-3xl uppercase">
                            An error occurred
                        </div>
                    ) : (
                        <div className="grid grid-cols-5 mb-20 gap-6 mt-20 w-full px-20">
                            {searchCardData}
                        </div>
                    )}
                </div>
                <MoviesFooter />
            </div>
        </div>
    );
};

export default MoviesSearch;
