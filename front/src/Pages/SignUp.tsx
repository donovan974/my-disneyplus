import { useEffect, useRef, useState } from "react";
import { IoWarningOutline } from "react-icons/io5";
import { Link, useLocation, useNavigate } from "react-router-dom";
import DisneyLogo from "../assets/disneyplus.svg";
import { useAuthContext } from "../Context/AuthContext";
import { SignUp as SUP } from "../Request/AuthRequest";

type SignUpDetails = {
    email: string;
    password: string;
    cpassword: string;
    username: string;
};

function SignUp() {
    const { auth, setAuth } = useAuthContext();
    const navigate = useNavigate();
    const signDetails = useRef<SignUpDetails>({
        email: "",
        password: "",
        cpassword: "",
        username: "",
    });
    const location = useLocation();

    const [message, setMessage] = useState<string>(
        (location.state as string) || ""
    );

    useEffect(() => {
        auth.token !== "" && navigate("/movies");
    }, []);

    const inputHandler =
        (itemName: "email" | "password" | "username" | "cpassword") =>
        (e: React.ChangeEvent<HTMLInputElement>) => {
            signDetails.current[itemName] = e.target.value;
            setMessage("");
        };

    const ClickHandler = () => {
        if (signDetails.current.password !== signDetails.current.cpassword) {
            setMessage("Passwords need to be the same.");
            return;
        }
        SUP(
            signDetails.current.email,
            signDetails.current.password,
            signDetails.current.username
        ).then((data) => {
            if (data.token === "") {
                setMessage(
                    "An error occurred, try to reload the page. if the problem persist contact an admin"
                );
                return;
            }
            setAuth(data.token, data.user_id, data.username);
            navigate("/movies");
        });
    };

    return (
        <div className="w-full h-screen overflow-hidden flex justify-center items-start">
            <div className="w-[23rem] space-y-8 text-white mt-12">
                <div className="w-full flex justify-center items-center">
                    <Link to="/home">
                        <img
                            alt="Disney Logo"
                            className="h-[5.5rem]"
                            src={DisneyLogo}
                        />
                    </Link>
                </div>
                <div className="text-2xl leading-7 font-semibold">
                    Enregistrez-vous avec votre adresse e-mail
                </div>
                <div
                    className={`${
                        message !== "" ? "block" : "hidden"
                    } w-full py-2 bg-yellow-200 text-red-500 font-semibold px-2 border-black border-2 flex items-center`}
                >
                    <IoWarningOutline className="mr-2 text-xl stroke-2" />
                    {message}
                </div>
                <input
                    type="text"
                    placeholder="Username"
                    className="outline-none w-full bg-[#31343E] p-3"
                    onChange={inputHandler("username")}
                />
                <input
                    type="text"
                    placeholder="Adress e-mail"
                    className="outline-none w-full bg-[#31343E] p-3"
                    onChange={inputHandler("email")}
                />
                <input
                    type="password"
                    placeholder="Password"
                    className="outline-none w-full bg-[#31343E] p-3"
                    onChange={inputHandler("password")}
                />
                <input
                    type="password"
                    placeholder="Confirm Password"
                    className="outline-none w-full bg-[#31343E] p-3"
                    onChange={inputHandler("cpassword")}
                />
                <div
                    className="py-3 bg-[#068DFF] cursor-pointer font-semibold text-center rounded-[0.25rem] text-white uppercase text-xl"
                    onClick={ClickHandler}
                >
                    SE CONNECTER
                </div>
                <p>
                    Dejà un compte Disney+ ?{" "}
                    <Link to="/signin" className="underline">
                        Se connecter
                    </Link>
                </p>
            </div>
        </div>
    );
}

export default SignUp;
