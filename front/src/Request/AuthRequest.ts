import axios from "axios";

type TokenProps = {
    token: string;
    user_id: string;
    username: string;
};

const BaseEndpointApi = process.env.REACT_APP_BASE_API_URL;

export const SignIn = async (email: string, password: string) => {
    const token = await axios({
        method: "POST",
        url: `${BaseEndpointApi}/api/auth/signin`,
        headers: {
            "Content-Type": "application/json",
        },
        data: {
            email,
            password,
        },
    })
        .then((data: any) => {
            const obj = data.data.data;
            return {
                token: obj.access_token,
                user_id: obj.user_id,
                username: obj.username,
            } as TokenProps;
        })
        .catch((err) => ({ token: "", user_id: err } as TokenProps));
    return token;
};

export const SignUp = async (
    email: string,
    password: string,
    username: string
) => {
    const token = await axios({
        method: "POST",
        url: `${BaseEndpointApi}/api/auth/signup`,
        headers: {
            "Content-Type": "application/json",
        },
        data: {
            email,
            password,
            username,
        },
    })
        .then((data: any) => {
            const obj = data.data.data;

            return {
                token: obj.access_token,
                user_id: obj.user_id,
                username: obj.username,
            } as TokenProps;
        })
        .catch((err) => ({ token: "", user_id: err } as TokenProps));
    return token;
};

export const signOut = async () => {
    const token = localStorage.getItem("access_token");
    return axios({
        method: "POST",
        url: `${BaseEndpointApi}/api/auth/signout`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    });
};
