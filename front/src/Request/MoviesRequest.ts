import axios, { AxiosError } from "axios";

const BaseEndpointApi = process.env.REACT_APP_BASE_API_URL;

export type ErrorApiProps = {
    error_message: string;
    error_code: number;
};

export const getPopularFilm = async () => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "GET",
        url: `${BaseEndpointApi}/api/movies/popular`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    })
        .then((data: any) => data.data.data.results as any[])
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);

            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    return data;
};

export const getTopFilm = async () => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "GET",
        url: `${BaseEndpointApi}/api/movies/high-rated`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    })
        .then((data: any) => data.data.data.results as any[])
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);

            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    return data;
};

export type MovieDetailsProps = {
    backdrop_path: string;
    poster_path: string;
    genres: { id: number; name: string }[];
    overview: string;
    title: string;
    release_date: string;
    production_companies: { id: number; logo_path: string; name: string }[];
    vote_average: number;
};
export const getMovieDetail = async (film_id: number) => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "GET",
        url: `${BaseEndpointApi}/api/movies/detail/${film_id}`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    })
        .then((data: any) => data.data.data as MovieDetailsProps)
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);

            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    console.log();

    return data;
};

export type MovieSimilarProps = {
    backdrop_path: string;
    title: string;
    id: number;
};
export const getSimilarMovie = async (film_id: number) => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "GET",
        url: `${BaseEndpointApi}/api/movies/similar/${film_id}`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    })
        .then((data: any) => data.data.data.results as MovieSimilarProps[])
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);

            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    console.log();

    return data;
};

export type MovieSearchProps = {
    backdrop_path: string;
    poster_path: string;
    title: string;
    id: number;
};
export const getSearchMovie = async (query: string) => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "GET",
        url: `${BaseEndpointApi}/api/movies/search/${query}`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    })
        .then((data: any) => data.data.data.results as MovieSearchProps[])
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);

            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    console.log();

    return data;
};
