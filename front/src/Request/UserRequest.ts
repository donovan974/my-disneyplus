import axios, { AxiosError } from "axios";
import { ErrorApiProps } from "./MoviesRequest";

const BaseEndpointApi = process.env.REACT_APP_BASE_API_URL;

export type WatchListProps = { film_id: number; image: string; title: string };

export const getWatchList = async () => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "GET",
        url: `${BaseEndpointApi}/api/user/watchlist/me`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
    })
        .then((data: any) => data.data.data as WatchListProps[])
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);
            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    return data;
};

export const addWatchList = async (
    film_id: number,
    image: string,
    title: string
) => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "POST",
        url: `${BaseEndpointApi}/api/user/watchlist`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
        data: {
            film_id,
            image,
            title,
        },
    })
        .then(() => true)
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);
            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    return data;
};

export const deleteWatchList = async (film_id: number) => {
    const token = localStorage.getItem("access_token");
    const data = await axios({
        method: "DELETE",
        url: `${BaseEndpointApi}/api/user/watchlist`,
        headers: {
            "Content-Type": "application/json",
            token: token || "",
        },
        data: {
            film_id,
        },
    })
        .then(() => true)
        .catch((reason: AxiosError) => {
            const err = reason.response;
            console.log(err?.status);
            return {
                error_code: err?.status,
                error_message: err?.data.message,
            } as ErrorApiProps;
        });
    return data;
};
