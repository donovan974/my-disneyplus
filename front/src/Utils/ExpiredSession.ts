import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../Context/AuthContext";

export const useExpiredSession = () => {
    const navigate = useNavigate();
    const { auth, setAuth } = useAuthContext();

    const expireSession = () => {
        console.log("trying to to this");

        setAuth("", "", "");
        navigate("/signin", {
            state: "Your session as expired. Sign in to continue.",
        });
    };

    return expireSession;
};
