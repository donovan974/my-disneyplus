import { useEffect, useState } from "react";

export const useLocalStorage = (itemName: string) => {
    const [data, setData] = useState(localStorage.getItem(itemName) || "");

    const insertData = (value: string) => {
        localStorage.setItem(itemName, value);
        refreshData();
    };

    const refreshData = () => {
        const item = localStorage.getItem(itemName);
        setData(item || "");
    };

    useEffect(() => {
        refreshData();
    }, []);

    return [data, insertData] as [string, (val: string) => void];
};
