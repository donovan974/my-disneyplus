import { useEffect, useState } from "react";
import { CardContent } from "../Components/CardContent";
import DisneyCard from "../assets/disneyCard.png";
import {
    getMovieDetail,
    getPopularFilm,
    getSearchMovie,
    getSimilarMovie,
    getTopFilm,
    MovieDetailsProps,
    MovieSearchProps,
    MovieSimilarProps,
} from "../Request/MoviesRequest";

export type requestStatus = "LOAD" | "DONE" | "ERROR";

export const usePopularMovie = () => {
    const [data, setData] = useState<any[]>([]);
    const [status, SetStatus] = useState<requestStatus>("LOAD");
    const [id, setId] = useState<NodeJS.Timeout>({} as NodeJS.Timeout);

    useEffect(() => {
        refresh();
        return () => clearTimeout(id);
    }, []);

    const refresh = async () => {
        SetStatus("LOAD");
        const filmRawData = await getPopularFilm();
        if ("error_code" in filmRawData) {
            SetStatus("ERROR");
            return;
        }

        const films = filmRawData.map((item) => ({
            id: item.id as number,
            src: "https://image.tmdb.org/t/p/w500/" + item.backdrop_path,
            title: item.title,
        }));
        setData(() => {
            setId(setTimeout(() => SetStatus("DONE"), 150));
            return films;
        });
    };

    return [data, status, refresh] as [any[], requestStatus, () => void];
};

export const useTopRatedMovie = () => {
    const [data, setData] = useState<any[]>([]);
    const [status, SetStatus] = useState<requestStatus>("LOAD");
    const [id, setId] = useState<NodeJS.Timeout>({} as NodeJS.Timeout);

    useEffect(() => {
        refresh();
        return () => clearTimeout(id);
    }, []);

    const refresh = async () => {
        SetStatus("LOAD");
        const filmRawData = await getTopFilm();
        if ("error_code" in filmRawData) {
            SetStatus("ERROR");
            return;
        }
        const films = filmRawData.map((item) => ({
            id: item.id as number,
            src: "https://image.tmdb.org/t/p/w500/" + item.backdrop_path,
            title: item.title,
        }));
        setData(() => {
            setId(setTimeout(() => SetStatus("DONE"), 150));
            return films;
        });
    };

    return [data, status, refresh] as [any[], requestStatus, () => void];
};

export const useMovieDetail = (movie_id: number) => {
    const [data, setData] = useState<MovieDetailsProps>(
        {} as MovieDetailsProps
    );
    const [status, SetStatus] = useState<requestStatus>("LOAD");
    const [id, setId] = useState<NodeJS.Timeout>({} as NodeJS.Timeout);

    useEffect(() => {
        refresh();
        return () => clearTimeout(id);
    }, []);

    const refresh = async () => {
        SetStatus("LOAD");
        const filmRawData = await getMovieDetail(movie_id);
        if ("error_code" in filmRawData) {
            SetStatus("ERROR");
            return;
        }
        setData(() => {
            setId(setTimeout(() => SetStatus("DONE"), 150));
            return filmRawData;
        });
    };

    return [data, status, refresh] as [
        MovieDetailsProps,
        requestStatus,
        () => void
    ];
};

export const useSimilarMovie = (movie_id: number) => {
    const [data, setData] = useState<CardContent>([]);
    const [status, SetStatus] = useState<requestStatus>("LOAD");
    const [id, setId] = useState<NodeJS.Timeout>({} as NodeJS.Timeout);

    useEffect(() => {
        refresh();
        return () => clearTimeout(id);
    }, []);

    const refresh = async () => {
        SetStatus("LOAD");
        const filmRawData = await getSimilarMovie(movie_id);
        if ("error_code" in filmRawData) {
            SetStatus("ERROR");
            return;
        }
        setData(() => {
            setId(setTimeout(() => SetStatus("DONE"), 150));
            const film = filmRawData as MovieSimilarProps[];
            return film.map((item) => ({
                id: item.id,
                src: "https://image.tmdb.org/t/p/w500/" + item.backdrop_path,
                title: item.title,
            }));
        });
    };

    return [data, status, refresh] as [CardContent, requestStatus, () => void];
};

export const useSearchMovie = () => {
    const [data, setData] = useState<CardContent>([]);
    const [status, SetStatus] = useState<requestStatus>("LOAD");
    const [id, setId] = useState<NodeJS.Timeout>({} as NodeJS.Timeout);

    useEffect(() => {
        return () => clearTimeout(id);
    }, []);

    const refresh = async (query: string) => {
        SetStatus("LOAD");
        const filmRawData = await getSearchMovie(query);
        if ("error_code" in filmRawData) {
            SetStatus("ERROR");
            return;
        }

        setData(() => {
            setId(setTimeout(() => SetStatus("DONE"), 150));
            const film = filmRawData as MovieSearchProps[];
            return film.map((item) => {
                const imageExistEnce = item.backdrop_path
                    ? item.backdrop_path
                    : item.poster_path
                    ? item.poster_path
                    : false;
                const full_image =
                    imageExistEnce === false
                        ? DisneyCard
                        : "https://image.tmdb.org/t/p/w500/" + imageExistEnce;
                return {
                    id: item.id,
                    src: full_image,
                    title: item.title,
                };
            });
        });
    };

    return [data, status, refresh] as [
        CardContent,
        requestStatus,
        (query: string) => void
    ];
};

export type PosterObject = {
    poster_path: string;
    backdrop_path: string;
};
export const getPosterImage = (
    obj: PosterObject,
    baseUrl: string = "https://image.tmdb.org/t/p/w1280/"
) => {
    const img = obj.backdrop_path
        ? obj.backdrop_path
        : obj.poster_path
        ? obj.poster_path
        : null;
    return img === null ? DisneyCard : baseUrl + img;
};
